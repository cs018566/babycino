class TestFeatureC1 {
    public static void main(String[] a) {
	     System.out.println(new TestC1().f(4, 3));
    }
}

class TestC1 {

    public int f(int x, int y) {
      int result;

      if(x <= y)
        result = 1;
      else
        result = 2;

	    return result;
    }

}
